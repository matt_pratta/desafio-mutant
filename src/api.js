// Imports
const util = require('util')
const axios = require('axios')

// Classe que utilizaremos com funções helpers e controladores para funcionar de interface com a web
class api {
    // Função que configura o client
    setClient (client) {
        if (client)
            api.elasticsearchClient = client
    }

    // Função de logging personalizada
    log () {
        // Horário do log
        let logTime = new Date()

        // Convertemos os argumentos em array, convertemos em strings e depois combinamos tudo com espaços entre cada item
        let logMsg = Array.prototype.slice.call(arguments)
            .map(obj => {
                // Quando for string ou número, retornamos ele cru
                if (typeof(obj) == 'string' || typeof(obj) == 'number')
                    return obj

                // Em outros casos (arrays, objetos, etc), retornamos um util.inspect, que vai retornar o valor em JSON
                return util.inspect(obj)
            })
            .join(' ')

        // Imprime no console
        console.log(`[${logTime}] ${logMsg}`)

        // Loga no Elasticsearch
        api.elasticsearchClient.index({
            index: 'logs',
            body: {
                time: logTime,
                msg: logMsg
            }
        }).catch(e => console.error(e))
    }

    // Helper para obter os users
    getUsers () {
        return axios.get('https://jsonplaceholder.typicode.com/users')
            .then(res => res.data)
    }

    // Helper para filtrar array por key
    sortArrayByKey (array, key) {
        // Filtramos a array primeiro para deixar apenas itens que não sejam nulos ou indefinidos
        let validElements = array.filter(element => !!element)

        // Realizamos o sort
        validElements.sort((elementA, elementB) => {
            if (elementA[key] == elementB[key]) return 0
            if (elementA[key] > elementB[key]) return 1
            return -1
        })

        // Retornamos a array com itens válidos
        return validElements
    }
}

// Exportamos uma instância da classe para uso na aplicação
module.exports = new api()