// Imports
const api = require('./api')
const express = require('express')
const elasticsearch = require('@elastic/elasticsearch')

// Configura o Express
const app = express()
const port = 8080

try {
    let elasticsearchClient = new elasticsearch.Client({ node: 'http://localhost:9200' })

    api.setClient(elasticsearchClient)

    // Middleware para logar as requests no console e no Elasticsearch
    app.use((req, res, next) => {
        next()
        api.log(res.statusCode, req.method, req.url)
    })

    // Utilizaremos um arquivo de rotas à parte, para manter o código mais limpo
    const routes = require('./routes')
    app.use(routes)

    // Inicializa o Express
    app.listen(port, () => { api.log(`Servidor inicializado na porta ${port}.`) })
} catch (err) {
    console.error('Erro ao inicializar servidor:', err)
}