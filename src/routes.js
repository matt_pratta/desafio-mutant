const api = require('./api')
const Routes = module.exports = require('express').Router()

// Rota principal, retorna todos usuários
Routes.get('/', async (req, res) => {
    // Obtemos os usuários
    let users = await api.getUsers()
    
    // Retornamos todos usuários
    res.send(users)
})

// Rota websites, retorna todos os websites de todos usuários
Routes.get('/websites', async (req, res) => {
    // Obtemos os usuários
    let users = await api.getUsers()

    // Usamos map para retornar apenas os websites
    let websites = users.map(user => user.website)

    // Retornamos todos websites
    res.send(websites)
})

// Rota companies, retorna nome, e-mail e empresa dos usuários em ordem alfabética
Routes.get('/companies', async (req, res) => {
    // Obtemos os usuários
    let users = await api.getUsers()

    // Usamos map para processar os usuários
    let userInfos = users.map(user => {
        return {
            name: user.name,
            email: user.email,
            company: user.company
        }
    })

    // Realizamos o sort da array
    userInfos = api.sortArrayByKey(userInfos, 'name')

    // Retornamos os dados dos usuários
    res.send(userInfos)
})

// Rota suites, retorna todos usuários que possuam a palavra 'suite' no endereço
Routes.get('/suites', async (req, res) => {
    // Obtemos os usuários
    let users = await api.getUsers()

    // Usamos filter para filtrar os resultados
    let suites = users.filter(user => (user.address && user.address.suite && ~user.address.suite.toLowerCase().indexOf('suite')))

    // Retornamos todos que possuem suites
    res.send(suites)
})