# Boas-vindas :)
echo "Setup: Bem-vindo!"

# Configura o APT para dar preferência ao mirror mais próximo, ao invés dos padrões dos EUA, para uma configuração mais rápida
sudo echo "deb mirror://mirrors.ubuntu.com/mirrors.txt bionic main restricted universe multiverse" >> sources.list
sudo echo "deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-updates main restricted universe multiverse" >> sources.list
sudo echo "deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-backports main restricted universe multiverse" >> sources.list
sudo echo "deb mirror://mirrors.ubuntu.com/mirrors.txt bionic-security main restricted universe multiverse" >> sources.list
sudo echo "##" >> sources.list
sudo cat /etc/apt/sources.list >> sources.list
sudo mv sources.list /etc/apt/sources.list

# Configura repositório do Elasticsearch no APT
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

# Instala o Node.js, npm e Elasticsearch
echo "Setup: Instalando dependências do sistema"
sudo apt-get update
sudo apt-get install -y nodejs npm elasticsearch

# Remove a instalação anterior caso ela já exista
cd /
if [ -d "app" ] 
then
    echo "Setup: Limpando instalação existente..."
    rm -rf app
fi

# Copia o código da aplicação para a pasta ~/app
echo "Setup: Copiando aplicação..."
cp -R /vagrant/src app

# Navega para a pasta ~/app e instala os pacotes do Node.js
echo "Setup: Instalando aplicação..."
cd app
npm install

# Configura inicialização do Elasticsearch ao iniciar o sistema
sudo systemctl enable elasticsearch.service

# Inicializa o Elasticsearch
echo "Setup: Inicializando Elasticsearch..."
sudo service elasticsearch start

# Configura a aplicação para iniciar com o sistema via crontab
(crontab -l 2>/dev/null; echo "@reboot /usr/bin/node /app/index.js") | crontab -

# Aguarda 30 segundos até o servidor do elasticsearch iniciar
sleep 30

# Imprime documentação
echo "Setup: Instalação concluída!"
echo "----------------------------"
echo "Lista de rotas disponíveis:"
echo "/           -> Lista todos os usuários"
echo "/websites   -> Lista os websites de todos os usuários"
echo "/companies  -> Lista nome, e-mail e a empresa dos usuários, em ordem alfabética"
echo "/suites     -> Lista todos os usuários que possuem 'suite' no endereço"
echo "----------------------------"

# Inicializa aplicação
echo "Setup: Inicializando aplicação..."
/usr/bin/node index.js &